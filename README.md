# rb5-flight-px4-build-docker

Create a Docker image to build PX4 for the rb5-flight platform

## Getting started

The build environment for PX4 on RB5 Flight is only supported on Ubuntu 20.04 LTS (Focal Fossa).

PX4 runs partially on the Sensor Low Power Island (SLPI) DSP (aka sdsp) and partially
on the application processor (ARM64 / aarch64). In order to build for this platform both
the Qualcomm Hexagon (DSP) toolchain and the Linaro ARM64 toolchain need to be installed.
The (nearly) complete setup including the ARM64 toolchain are provided in the base Docker image
provided by ModalAI, but since ModalAI is not allowed to redistribute the Qualcomm
Hexagon DSP SDK this must be added by the end user. This document explains how to
pull all the pieces together to complete the Docker image.

### Install Qualcomm Package Manager

In order to install the Hexagon SDK the user must have an account with Qualcomm.

- Set up an account on the Qualcomm Developer Network: https://developer.qualcomm.com/

The Hexagon SDK is installed via the Qualcomm Package Manager. Install that:

- Go to Downloads, Software Downloads
- Choose Compilers, Qualcomm Package Manager - Linux
- Untar qualcommpackagemanager.lnx_.2.0_installer_20021.2.tar
- Read through the enclosed documentation
- Unfortunately, the documentation is not correct for Linux Ubuntu 20.04.
- There is a document on Qualcomm's createpoint site that explains some of the issues:
  - KBA-210421083747 How to install QPM in Ubuntu 18.04 & 20.04
- But even that document isn't complete. Here are the correct steps:
  - sudo apt-get update
  - sudo apt-get upgrade
  - sudo apt --fix-broken install
  - sudo apt-get install xterm
  - wget http://launchpadlibrarian.net/416685704/multiarch-support_2.19-0ubuntu6.15_amd64.deb
  - wget http://launchpadlibrarian.net/201380610/libgnome-keyring0_3.12.0-1build1_amd64.deb
  - wget http://launchpadlibrarian.net/201380608/libgnome-keyring-common_3.12.0-1build1_all.deb
  - sudo dpkg -i multiarch-support_2.19-0ubuntu6.15_amd64.deb
  - sudo dpkg -i libgnome-keyring-common_3.12.0-1build1_all.deb
  - sudo dpkg -i libgnome-keyring0_3.12.0-1build1_amd64.deb
  - sudo dpkg -i QualcommPackageManager.2.0.21.1.Linux-x86.deb

### Install and archive Hexagon SDK version 4.1.0.4

- Run the QualcommPackageManager (e.g. "qpm" or "sudo qpm" from the command line)
- Wait for the GUI to launch (Be patient, this can take some time)
- Go to Hexagontm SDK 4.x and choose 4.1.0.4 in the drop down menu and install
  - After another long wait you will see the installation start. Make sure
    to install into a location that doesn't require sudo unless you ran qpm as sudo.
- Assuming the default installation location, go to /local/mnt/workspace/Qualcomm/Hexagon_SDK
- "Tar up" the SDK (e.g. "tar -czvf sdk.tgz 4.1.0.4/")

### Download and load the base Docker image from ModalAI

- Use setup.sh to retrieve and load the base Docker image

## Build the Docker image

- Make sure the Hexagon SDK archive is in the project directory
- Run build.sh

## Use Docker image to build PX4

- Use the high level wrapper project to build PX4: https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4
