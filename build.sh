#!/bin/bash

# Build the full build environment Docker
docker build -t rb5-flight-px4-build-docker:v1.5 .
docker tag rb5-flight-px4-build-docker:v1.5 rb5-flight-px4-build-docker:latest
