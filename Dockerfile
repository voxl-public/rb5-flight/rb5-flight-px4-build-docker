
FROM base-px4-build-docker:v1.4

WORKDIR /home

# Setup the tools to build the SLPI DSP code and the Apps ARM code
COPY sdk.tgz .

RUN tar -xzf sdk.tgz

RUN rm sdk.tgz

RUN mv gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/ /home/4.1.0.4/tools/

RUN mv /home/4.1.0.4/tools/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu /home/4.1.0.4/tools/linaro64

WORKDIR /usr/local/workspace

ENV PATH="/home/4.1.0.4/tools/linaro64/bin:${PATH}"

CMD /bin/bash
