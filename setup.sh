#!/bin/bash

# Get the base Docker image from ModalAI. This has everything except for the Hexagon SDK.
wget https://storage.googleapis.com/modalai_public/rb5-flight/base-px4-build-docker/base-px4-docker-image_1.4.tgz

# Load the base Docker image
docker load < base-px4-docker-image_1.4.tgz
